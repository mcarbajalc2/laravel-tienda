/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var login = {
    init: function(){
        $(document).on('click','.btn-login',function(e){
            e.preventDefault();
            login.open();
        });
        $(document).on('click','#login',function(e){
            e.preventDefault();
            var id = $(e.target).attr("id");
            if(id === "login"){
                login.close();
            }
        });
    },
    open: function(){
        $("#login").removeClass("d-none");
        $("#login").addClass("d-flex");
    },
    close: function(){
        $("#login").removeClass("d-flex");
        $("#login").addClass("d-none");
    }
};

$(document).ready(function(){
    login.init();
});
