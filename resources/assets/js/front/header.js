var main_menu = {	
	evt: {
		/*EVENTOS*/
		manage: function(e){
                        if($(".header .main-menu").hasClass("active")){
                                /*Si esta activo cerrar*/
                                main_menu.evt.close();
                        }else{
                                /*Si no abrir*/
                                main_menu.evt.open();
                        }

		},
		open: function(){
			$(".header .main-menu-btn").siblings(".main-menu").addClass('active');
		},
		close: function(){
                        $(".header .main-menu").addClass("hide");
                        setTimeout(function(){
                            $(".header .active").removeClass("active")
                            $(".header .hide").removeClass("hide")
                        },500);                        
		}
	}
};

var sub_menu = {
        /*EVENTOS*/
        evt: {
                open: function(e){
                    var li = $(e.target).parents("li")[0];
                    $(li).addClass("active");
                },
                close: function(e){
                    var li = $(e.target).parents("li")[0];
                    $(li).addClass("hide");
                    setTimeout(function(){
                        $(li).removeClass("active");
                        $(li).removeClass("hide");
                    },500);
                }
        }
};

$(document).ready(function() {
    /*ESCUCHAS MENU*/
    $(".header .main-menu-btn").on("click",function(e){
            e.preventDefault();
            main_menu.evt.manage();
    });
    
    $(".header .main-menu").on("click",function(e){
            e.preventDefault();
            if($(e.target).hasClass("main-menu")){
                main_menu.evt.close();
            }
    });
    
    /*ESCUCHAS SUBMENU*/
    $(".header .main-menu>.menu-content>ul>li>a").on("click",function(e){
            e.preventDefault();
            sub_menu.evt.open(e);
    });
    
    $(".header .main-menu .submenu-content>.submenu-header>.goback").on("click",function(e){
            e.preventDefault();
            sub_menu.evt.close(e);
    });
});
