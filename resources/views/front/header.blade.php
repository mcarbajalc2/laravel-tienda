@inject('categories','App\Category')
@php
    $cats = $categories::whereNull('parent_id')->whereNotNull('visible')->get();
@endphp

<div class="container d-flex align-items-center">
    <a href="" class="logo d-block">
            <img class="w-100" src="{{ asset('images/logo-x.png') }}" alt="">
    </a>
    <nav class="main-menu ml-auto mr-0">
            <div class="menu-content d-flex flex-column position-relative">
                    <div class="top d-flex flex-row align-items-center justify-content-around">
                        <button class="btn btn-login">Iniciar Sesión</button>
                        <button class="btn btn-signup">Registrarse</button>
                    </div>
                    <ul class="d-flex p-0 flex-column w-100 items">
                        @foreach($cats as $cat)
                        @php
                            $cat->description = json_decode($cat->description)->$LANG;                         
                        @endphp
                        <li>
                            <a href="#">{{$cat->description}}</a>
                            @if((count($cat->categories)))
                                <div class="submenu-content">
                                    <div class="submenu-header d-flex align-items-center">
                                        <button class="goback"><i class="fa fa-fw fa-angle-left fa-2x"></i></button>
                                        <h5>{{$cat->description}}</h5>
                                    </div>
                                    <ul class="p-0 m-0">
                                        @foreach($cat->categories as $sub_cat)
                                            @php
                                                $sub_cat->description = json_decode($sub_cat->description)->$LANG;  
                                            @endphp
                                            <li><a href="#">{{$sub_cat->description}}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif                            
                        </li>
                        @endforeach
                        <!--
                        <li>
                            <a href="#">Libros</a>
                            <div class="submenu-content">
                                <div class="submenu-header d-flex align-items-center">
                                    <button class="goback"><i class="fa fa-fw fa-angle-left fa-2x"></i></button>
                                    <h5>Libros</h5>
                                </div>
                                <ul class="p-0 m-0">
                                    <li><a href="#">Matemáticas</a></li>
                                    <li><a href="#">Física</a></li>
                                    <li><a href="#">Química</a></li>
                                    <li><a href="#">Ingenieria</a></li>
                                    <li><a href="#">Derecho</a></li>
                                </ul>
                            </div>
                        </li>
                        <li><a href="#">Arquitectura</a></li>
                        <li><a href="#">Ingenieria</a></li>
                        <li><a href="#">Oficina</a></li>
                        <li><a href="#">Utiles</a></li>
                        -->
                    </ul>
            </div>
    </nav>
    <button class="ml-auto main-menu-btn">
            <label></label>
            <label></label>
            <label></label>
    </button>
</div>
