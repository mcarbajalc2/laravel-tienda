<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Tienda</title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/front/app.css')}}"/>
</head>
<body>
	<!-- HEADER -->
        <div id="login" class="d-none popup position-fixed w-100 h-100 justify-content-center align-items-center">
            <login></login>
        </div>
	<header class="header">
            @component('front/header')
            @endcomponent
	</header>
        <section>
            @yield('content')
        </section>
        <footer>
            
        </footer>
        <script src="{{asset('js/front/app.js')}}"></script>
</body>
</html>