@extends('front/master')
@section('content')

<a href="#" class="banner-offerts">
    <div class="banner-caption-container w-100 h-100 d-flex justify-content-center align-items-center">
        <div class="banner-caption-content">
            <img src="{{asset('images/portada_msg.jpg')}}" alt="">
            <h3 class="text-center d-none">
                15% de descuento <br>
                <small>Compra por nuestra página y obten tu descuento.</small>
            </h3>            
            <div class="call-to-action text-center d-none">
                <button class="btn btn-primary">VER OFERTAS</button>
            </div>
        </div>
    </div>
</a>
<div class="products-container">
    <div class="flex-column cards-container">
        <div class="col-12 card">
            <div class="card-header">
                <h3>Destacados</h3>
            </div>
            <div class="card-content">
                @foreach($highlights as $item)
                <a href="{{ url(json_decode($item->friendly_url,true)[$LANG]) }}" class="item col-12 d-flex">
                    <div class="img-container w-50">
                        <img class="w-100" src="{{ asset('images/products/'.$item->images[0]->url) }}" alt="{{ json_decode($item->images[0]->title, true)[$LANG] }}">
                    </div>
                    <div class="item-container d-flex flex-column w-100 justify-content-center">
                        <h5>{{ json_decode($item->name, true)[$LANG] }}</h5>
                        <span class="price">S./{{ number_format($item->price,2,'.',',') }}</span>
                    </div>
                </a>
                @endforeach
            </div>
            <a href="#" class="card-footer d-flex align-items-center">
                <span>Ver mas...</span>
                <i class="fa fa-angle-right ml-auto"></i>
            </a>
        </div>
        <div class="card-slide card">
            <div class="box-x drag-x d-flex">
                item
            </div>
        </div>
        
    </div>
</div>
@stop

