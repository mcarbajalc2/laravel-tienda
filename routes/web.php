<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::namespace('Frontend')->group(function(){
    // Controllers Within The "App\Http\Controllers\Frontend" Namespace
    Route::get('index/',"HomeController@index");
    Route::get('/',"HomeController@index");
    Route::post('login/','AuthController@login');
});

Route::namespace('Backend')->group(function () {
    // Controllers Within The "App\Http\Controllers\Backend" Namespace
    Route::prefix('admin')->group(function(){
        Route::get('index/',"HomeController@index");
        Route::get('/',"HomeController@index");
    });    
});

Route::get('/home', 'HomeController@index')->name('home');
