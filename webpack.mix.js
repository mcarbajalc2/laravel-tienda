let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |

mix.js([
    'resources/assets/js/app.js',
    'resources/assets/plugins/jquery/jquery-3.2.1.min.js',    
    'resources/assets/plugins/jquery-ui/jquery-ui.min.js',
    'resources/assets/plugins/jquery/jquery-ui-touch-punch.min.js',    
    'resources/assets/plugins/bootstrap/js/bootstrap.bundle.min.js',
    'resources/assets/js/vue.js',
    'resources/assets/js/axios.js',
    'resources/assets/js/front/header.js' 
],'public/js/front/app.js');
   
mix.styles([
    'resources/assets/plugins/bootstrap/css/bootstrap.min.css',
    'resources/assets/plugins/font-awesome/css/font-awesome.min.css',
    'resources/assets/css/front/main.css'
],'public/css/front/app.css');
 */
mix.js([
        'resources/assets/js/app.js',
        'resources/assets/js/front/header.js',
        'resources/assets/js/front/main.js'
    ],'public/js/front/')
    .sass('resources/assets/sass/app.scss','public/css/front/');