<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->text('description');
            $table->text('long_description');
            $table->boolean('visible')->default(true);
            $table->double('quantity');
            $table->double('cost');
            $table->double('price');
            $table->integer('highlight')->default(0)->nullable();
            $table->text('friendly_url');
            
            $table->integer('brand_id')->unsigned()->nullable();
            $table->foreign('brand_id')
                    ->references('id')
                        ->on('brands');
            
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')
                    ->references('id')
                        ->on('categories');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
