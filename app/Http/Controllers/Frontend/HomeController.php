<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    //
    public function __construct(){
        parent::__construct();
    }
    
    public function index(){
        $data["highlights"] = \App\Product::limit(3)
                ->where("visible",true)
                ->where("highlight","<>","0")
                ->get();
        
        return view("front/home",$data);
    }
}
