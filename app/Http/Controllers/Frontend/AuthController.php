<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    protected $redirectTo = '/';
    //
    public function login(Request $request) {
        $email = $request->input("email");
        $password = $request->input("password");
        if (Auth::attempt(['email' => $email, 'password' => $password])){
            // Autenticado
            $user = auth()->user();
            return \Response::json($user);
        }
    }
}
