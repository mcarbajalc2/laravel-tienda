<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    
    public function categories(){
        return $this->hasMany('App\Category','parent_id');
    }
    
    public function category(){
        return $this->belongsTo('App\Category','id');
    }
    
    public function products(){
        return $this->hasMany('App\Product','category_id');
    }
}
