<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field_Group extends Model
{
    protected $table = 'field__groups';
    
    public function fields(){
        return $this->hasMany('App\Field','group_id');
    }
}
