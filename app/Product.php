<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected  $table = 'products';
    
    public function images(){
        return $this->hasMany('App\Image','product_id');
    }
    
    public function brand(){
        return $this->belongsTo('App\Brand','id');
    }
    
    public function category(){
        return $this->belongsTo('App\Brand','id');
    }
    
    public function tags(){
        return $this->belongsToMany('App\Tag');
    }
}
