<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $table = 'fields';
    
    public function field_groups(){
        return $this->belongsTo('App\Field_Group','id');
    }
}
